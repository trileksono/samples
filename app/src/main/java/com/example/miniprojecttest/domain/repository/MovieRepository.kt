package com.example.miniprojecttest.domain.repository

import com.example.miniprojecttest.data.model.response.MovieResponse
import com.example.miniprojecttest.data.source.network.MovieApi
import com.example.miniprojecttest.domain.cache.FavoriteCache
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface MovieRepository {
    fun getPopular(page: Int): Flow<State<List<Movie>>>
    fun getUpcoming(page: Int): Flow<State<List<Movie>>>
    fun getTopRated(page: Int): Flow<State<List<Movie>>>
    fun getNowPlaying(page: Int): Flow<State<List<Movie>>>
    fun getFavorite(): Flow<State<List<Movie>>>
    fun saveFavorite(movie: Movie): Flow<State<Any>>
    fun deleteFavorite(movie: Movie): Flow<State<Any>>
}

class MovieRepositoryImpl(
    private val movieApi: MovieApi,
    private val favoriteCache: FavoriteCache
) : MovieRepository {
    override fun getPopular(page: Int): Flow<State<List<Movie>>> = flow {
        emit(State.Loading)
        val state = try {
            val response = movieApi.getPopular(page)
            State.Success(response.results?.map { MovieResponse.mapToModel(it) }.orEmpty())
        } catch (e: Exception) {
            State.Error(e.message ?: "Something wrong, please try again later")
        }
        emit(state)
    }

    override fun getUpcoming(page: Int): Flow<State<List<Movie>>> = flow {
        emit(State.Loading)
        val state = try {
            val response = movieApi.getUpcoming(page)
            State.Success(response.results?.map { MovieResponse.mapToModel(it) }.orEmpty())
        } catch (e: Exception) {
            State.Error(e.message ?: "Something wrong, please try again later")
        }
        emit(state)
    }

    override fun getNowPlaying(page: Int): Flow<State<List<Movie>>> = flow {
        emit(State.Loading)
        val state = try {
            val response = movieApi.getNowPlaying(page)
            State.Success(response.results?.map { MovieResponse.mapToModel(it) }.orEmpty())
        } catch (e: Exception) {
            State.Error(e.message ?: "Something wrong, please try again later")
        }
        emit(state)
    }

    override fun getTopRated(page: Int): Flow<State<List<Movie>>> = flow {
        emit(State.Loading)
        val state = try {
            val response = movieApi.getTopRated(page)
            State.Success(response.results?.map { MovieResponse.mapToModel(it) }.orEmpty())
        } catch (e: Exception) {
            State.Error(e.message ?: "Something wrong, please try again later")
        }
        emit(state)
    }

    override fun getFavorite(): Flow<State<List<Movie>>> {
        return flow {
            emit(State.Loading)
            emit(State.Success(favoriteCache.findAllFavorite()))
        }
    }

    override fun saveFavorite(movie: Movie): Flow<State<Any>> {
        return flow {
            emit(
                try {
                    favoriteCache.insertFavorite(movie)
                    State.Success(System.currentTimeMillis())
                } catch (e: Exception) {
                    State.Error("Cannot insert favorite ${e.message}")
                }
            )
        }
    }

    override fun deleteFavorite(movie: Movie): Flow<State<Any>> {
        return flow {
            emit(
                try {
                    favoriteCache.deleteFavorite(movie)
                    State.Success(System.currentTimeMillis())
                } catch (e: Exception) {
                    State.Error("Cannot delete favorite ${e.message}")
                }
            )
        }
    }
}