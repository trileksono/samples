package com.example.miniprojecttest.domain.model

import android.os.Parcelable
import com.example.miniprojecttest.data.source.local.room.entity.FavoriteMovieEntity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val id: Int? = null,
    val posterPath: String? = null,
    val releaseDate: String? = null,
    val title: String? = null,
    val overview: String? = null,
    var isFavorite: Boolean? = false
) : Parcelable {
    companion object {
        fun mapToEntity(movie: Movie?): FavoriteMovieEntity {
            return FavoriteMovieEntity(
                movie?.id ?: 0,
                movie?.title,
                movie?.posterPath,
                movie?.releaseDate,
                movie?.overview
            )
        }
    }
}
