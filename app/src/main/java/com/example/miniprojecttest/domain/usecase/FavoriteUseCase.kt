package com.example.miniprojecttest.domain.usecase

import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow

interface FavoriteUseCase {
    fun insertFavorite(movie: Movie): Flow<State<Any>>
    fun deleteFavorite(movie: Movie): Flow<State<Any>>
    fun getAllFavorite(): Flow<State<List<Movie>>>
}

class FavoriteUseCaseImpl(private val movieRepository: MovieRepository) : FavoriteUseCase {
    override fun insertFavorite(movie: Movie): Flow<State<Any>> =
        movieRepository.saveFavorite(movie)

    override fun deleteFavorite(movie: Movie): Flow<State<Any>> =
        movieRepository.deleteFavorite(movie)

    override fun getAllFavorite(): Flow<State<List<Movie>>> = movieRepository.getFavorite()
}
