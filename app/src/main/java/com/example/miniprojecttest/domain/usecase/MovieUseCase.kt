package com.example.miniprojecttest.domain.usecase

import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.domain.repository.MovieRepository
import com.example.miniprojecttest.view.listmovie.item.CategoryType
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface MovieUseCase {
    fun getMoviePage(page: Int, type: CategoryType): Flow<State<List<Movie>>>
}

class MovieUseCaseImpl @Inject constructor(
    private val movieRepository: MovieRepository
) : MovieUseCase {

    override fun getMoviePage(page: Int, type: CategoryType): Flow<State<List<Movie>>> {
        return when (type) {
            CategoryType.POPULAR -> movieRepository.getPopular(page)
            CategoryType.NOW_PLAYING -> movieRepository.getNowPlaying(page)
            CategoryType.TOP_RATED -> movieRepository.getTopRated(page)
            CategoryType.UPCOMING -> movieRepository.getUpcoming(page)
        }
    }
}
