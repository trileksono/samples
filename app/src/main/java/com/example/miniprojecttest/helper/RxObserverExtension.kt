package com.example.miniprojecttest.helper

object TagInjection {
    const val UI_Scheduler = "TagInjection.UI_Scheduler"
    const val IO_Scheduler = "TagInjection.IO_Scheduler"
}
