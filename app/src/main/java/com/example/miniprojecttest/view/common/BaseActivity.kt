package com.example.miniprojecttest.view.common

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<VB : ViewBinding, VM : BaseViewModel> : DaggerAppCompatActivity() {

    private var mToolbar: Toolbar? = null

    protected var viewModel: VM? = null

    var viewBinding: VB? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = bindViewModel()
        viewBinding = getUiBinding()
        setContentView(viewBinding?.root)
        setupToolbar()
        onFirstLaunch(savedInstanceState)
        initUiListener()
        observeViewModel(viewModel)
    }

    abstract fun bindViewModel(): VM
    abstract fun enableBackButton(): Boolean
    abstract fun bindToolbar(): Toolbar?
    abstract fun getUiBinding(): VB
    abstract fun onFirstLaunch(savedInstanceState: Bundle?)
    abstract fun initUiListener()
    abstract fun observeViewModel(viewModel: VM?)

    override fun onDestroy() {
        super.onDestroy()
        viewBinding = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> false
    }

    private fun setupToolbar() {
        if (bindToolbar() == null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(enableBackButton())
        } else {
            bindToolbar()?.let {
                mToolbar = it
                setSupportActionBar(mToolbar)
                supportActionBar?.apply {
                    setDisplayShowTitleEnabled(false)
                    setDisplayHomeAsUpEnabled(enableBackButton())
                }
            }
        }
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}