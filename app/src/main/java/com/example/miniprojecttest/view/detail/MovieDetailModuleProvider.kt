package com.example.miniprojecttest.view.detail

import androidx.lifecycle.ViewModel
import com.example.miniprojecttest.di.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [MovieDetailModule::class])
abstract class MovieDetailModuleProvider {

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindMovieDetailVM(viewModel: MovieDetailViewModel): ViewModel
}
