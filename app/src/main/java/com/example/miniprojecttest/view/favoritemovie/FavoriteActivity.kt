package com.example.miniprojecttest.view.favoritemovie

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ActivityFavoriteBinding
import com.example.miniprojecttest.databinding.ItemMovieDetailBinding
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.view.common.BaseActivity
import com.example.miniprojecttest.view.detail.item.MovieDetailItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.coroutines.flow.collect

class FavoriteActivity : BaseActivity<ActivityFavoriteBinding, FavoriteViewModel>() {

    private var itemAdapter = ItemAdapter<MovieDetailItem>()
    private var fastAdapter = FastAdapter.with(itemAdapter)

    override fun getUiBinding(): ActivityFavoriteBinding {
        return ActivityFavoriteBinding.inflate(layoutInflater)
    }

    override fun enableBackButton(): Boolean = true

    override fun bindToolbar(): Toolbar? = null

    override fun onFirstLaunch(savedInstanceState: Bundle?) {
        viewBinding?.run {
            favoriteRecycler.let {
                it.layoutManager = LinearLayoutManager(this@FavoriteActivity)
                it.adapter = fastAdapter
            }
        }
    }

    override fun initUiListener() {
        fastAdapter.addEventHook(object : ClickEventHook<MovieDetailItem>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return if (viewHolder is BindingViewHolder<*> && viewHolder.binding is ItemMovieDetailBinding) {
                    (viewHolder.binding as ItemMovieDetailBinding).itemDetailLinearLayout
                } else {
                    null
                }
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<MovieDetailItem>,
                item: MovieDetailItem
            ) {
                if (v.id == R.id.itemDetailLinearLayout) {
                    viewModel?.deleteFavorite(item.movie, position)
                }
            }
        })
    }


    override fun observeViewModel(viewModel: FavoriteViewModel?) {
        lifecycleScope.launchWhenStarted {
            viewModel?.deleteFavorite?.collect {
                when (it) {
                    is State.Success -> {
                        val position = (it.data as Pair<Any, Int>)
                        itemAdapter.remove(position.second)
                    }
                    is State.Error -> showToast(it.message)
                    else -> {
                    }
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel?.favorites?.collect {
                when (it) {
                    is State.Success -> {
                        itemAdapter.add(
                            it.data.mapIndexed { index, movie ->
                                return@mapIndexed movie.apply { isFavorite = true }
                            }.map {
                                MovieDetailItem(it)
                            })
                    }
                    is State.Error -> showToast(it.message)
                    else -> {
                    }
                }
            }
        }
    }

    override fun bindViewModel(): FavoriteViewModel {
        return ViewModelProvider(this, viewModelFactory).get(FavoriteViewModel::class.java)
    }

    companion object {
        fun createIntent(caller: AppCompatActivity): Intent {
            return Intent(caller, FavoriteActivity::class.java)
        }
    }

}

