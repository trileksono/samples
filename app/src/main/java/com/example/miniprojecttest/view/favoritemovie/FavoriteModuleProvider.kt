package com.example.miniprojecttest.view.favoritemovie

import androidx.lifecycle.ViewModel
import com.example.miniprojecttest.di.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [FavoriteModule::class])
abstract class FavoriteModuleProvider {

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteViewModel::class)
    abstract fun bindFavoriteVM(viewModel: FavoriteViewModel): ViewModel
}
