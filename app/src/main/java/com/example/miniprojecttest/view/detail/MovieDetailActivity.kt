package com.example.miniprojecttest.view.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ActivityDetailBinding
import com.example.miniprojecttest.databinding.ItemMovieDetailBinding
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.view.common.BaseActivity
import com.example.miniprojecttest.view.detail.item.MovieDetailItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.item_movie_detail.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MovieDetailActivity : BaseActivity<ActivityDetailBinding, MovieDetailViewModel>() {

    private val itemAdapter = ItemAdapter<IItem<*>>()
    private val fastAdapter = FastAdapter.with(itemAdapter)

    private lateinit var movieArgs: Movie

    override fun bindViewModel(): MovieDetailViewModel {
        return ViewModelProvider(this, viewModelFactory).get(MovieDetailViewModel::class.java)
    }

    override fun enableBackButton(): Boolean = true

    override fun bindToolbar(): Toolbar? = null

    override fun getUiBinding(): ActivityDetailBinding {
        return ActivityDetailBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?) {
        movieArgs = intent.getParcelableExtra(EXTRA_MOVIE)!!
        viewBinding?.run {
            detailRecycler.let {
                it.layoutManager = LinearLayoutManager(this@MovieDetailActivity)
                it.adapter = fastAdapter
            }
        }

        itemAdapter.add(MovieDetailItem(movieArgs.also {
            it.isFavorite = viewModel?.favorites?.map { it.id }?.contains(it.id)
        }))
    }

    override fun initUiListener() {
        fastAdapter.addEventHook(object : ClickEventHook<GenericItem>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return if (viewHolder is BindingViewHolder<*> && viewHolder.binding is ItemMovieDetailBinding) {
                    (viewHolder.binding as ItemMovieDetailBinding).itemDetailLinearLayout
                } else {
                    null
                }
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<GenericItem>,
                item: GenericItem
            ) {
                if (v.id == R.id.itemDetailLinearLayout) {
                    val movie = (item as MovieDetailItem).movie
                    if (movie.isFavorite == true) {
                        viewModel?.deleteFavorite(movie)
                    } else {
                        viewModel?.insertFavorite(movie)
                    }
                }
            }
        })
    }

    override fun observeViewModel(viewModel: MovieDetailViewModel?) {
        lifecycleScope.launch {
            viewModel?.deleteFavorite?.collect {
                when (it) {
                    is State.Success -> {
                        fastAdapter.notifyAdapterItemChanged(
                            0, movieArgs.apply { isFavorite = false }
                        )
                    }
                    is State.Error -> showToast(it.message)
                    else -> {
                    }
                }
            }
        }

        lifecycleScope.launch {
            viewModel?.insertFavorite?.collect {
                when (it) {
                    is State.Success -> {
                        fastAdapter.notifyAdapterItemChanged(
                            0, movieArgs.apply { isFavorite = true }
                        )
                    }
                    is State.Error -> showToast(it.message)
                    else -> {
                    }
                }
            }
        }
    }

    companion object {
        private const val EXTRA_MOVIE = "MovieDetail.movie"
        fun createIntent(caller: AppCompatActivity, movie: Movie): Intent {
            return Intent(caller, MovieDetailActivity::class.java).apply {
                putExtra(EXTRA_MOVIE, movie)
            }
        }
    }
}
