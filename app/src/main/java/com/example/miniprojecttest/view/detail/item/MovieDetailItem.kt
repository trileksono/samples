package com.example.miniprojecttest.view.detail.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ItemMovieDetailBinding
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.helper.bindImage
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class MovieDetailItem(val movie: Movie) : AbstractBindingItem<ItemMovieDetailBinding>() {

    private val PREFIX_URl_IMAGE = "https://image.tmdb.org/t/p/w342"

    override val type: Int get() = R.id.item_movie_id

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemMovieDetailBinding {
        return ItemMovieDetailBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemMovieDetailBinding, payloads: List<Any>) {
        binding.run {
            itemDetailChkFavorite.isChecked = movie.isFavorite == true
            itemDetailImgPoster.bindImage(PREFIX_URl_IMAGE.plus(movie.posterPath))
            itemDetailTvTitle.text = movie.title
            itemDetailTvDateRelease.text = movie.releaseDate
            itemDetailTvOverview.text = movie.overview
        }
    }
}
