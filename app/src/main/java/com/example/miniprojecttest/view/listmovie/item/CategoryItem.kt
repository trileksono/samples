package com.example.miniprojecttest.view.listmovie.item

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ItemCategoryBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class CategoryItem(val category: Triple<String, CategoryType, Boolean>) :
    AbstractBindingItem<ItemCategoryBinding>() {

    override val type: Int get() = hashCode()

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemCategoryBinding {
        return ItemCategoryBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemCategoryBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.categoryTvName.text = category.first
        if (category.third) {
            binding.categoryTvName.setTextColor(
                ContextCompat.getColor(
                    binding.root.context,
                    R.color.purple_500
                )
            )
        }
    }
}

enum class CategoryType {
    POPULAR, UPCOMING, TOP_RATED, NOW_PLAYING
}
