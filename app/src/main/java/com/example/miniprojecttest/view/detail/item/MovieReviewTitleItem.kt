package com.example.miniprojecttest.view.detail.item

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ItemReviewTitleBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class MovieReviewTitleItem(val totalReview: Int) : AbstractBindingItem<ItemReviewTitleBinding>() {

    override val type: Int get() = R.id.item_loading_id

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ItemReviewTitleBinding {
        return ItemReviewTitleBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemReviewTitleBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.reviewTvReviewer.text = "$totalReview Review"
        binding.reviewTvShowAll.isVisible = totalReview > 0
    }
}
