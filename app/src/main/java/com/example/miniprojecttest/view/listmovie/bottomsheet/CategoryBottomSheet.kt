package com.example.miniprojecttest.view.listmovie.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.miniprojecttest.databinding.BottomsheetCategoryBinding
import com.example.miniprojecttest.view.listmovie.item.CategoryItem
import com.example.miniprojecttest.view.listmovie.item.CategoryType
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter

class CategoryBottomSheet(
    private val callback: (CategoryType) -> Unit,
    private val dismissCallback: () -> Unit
) : BottomSheetDialogFragment() {

    private val itemAdapter = ItemAdapter<CategoryItem>()
    private val fastAdapter = FastAdapter.with(itemAdapter)
    private lateinit var viewBinding: BottomsheetCategoryBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = BottomsheetCategoryBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initUiListener()
    }

    private fun initRecyclerView() {
        viewBinding.sortBottomSheetRecycler.run {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = fastAdapter
        }

        requireArguments().getSerializable(EXTRA_SORT_CONTENT)?.let {
            val lists = it as ArrayList<Triple<String, CategoryType, Boolean>>
            itemAdapter.add(lists.map { CategoryItem(it) })
        }
    }

    private fun initUiListener() {
        viewBinding.run {
            fastAdapter.onClickListener = { v, adapter, item, position ->
                callback(item.category.second)
                dismiss()
                false
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dismissCallback()
    }

    companion object {
        private const val EXTRA_SORT_CONTENT = "CategoryBottom.content"
        fun newInstance(
            content: ArrayList<Triple<String, CategoryType, Boolean>>,
            submitCallback: ((CategoryType) -> Unit) = { },
            dismissCallback: (() -> Unit) = { }
        ): BottomSheetDialogFragment {
            return CategoryBottomSheet(submitCallback, dismissCallback).apply {
                val bundle = Bundle()
                bundle.putSerializable(EXTRA_SORT_CONTENT, content)
                arguments = bundle
            }
        }
    }
}
