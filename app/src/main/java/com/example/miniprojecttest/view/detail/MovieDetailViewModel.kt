package com.example.miniprojecttest.view.detail

import androidx.lifecycle.viewModelScope
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.domain.usecase.FavoriteUseCase
import com.example.miniprojecttest.view.common.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private val favoriteUseCase: FavoriteUseCase
) : BaseViewModel() {
    private val _insertFavoriteFlow = MutableStateFlow<State<Any>>(State.Loading)
    val insertFavorite: StateFlow<State<Any>> = _insertFavoriteFlow

    private val _deleteFavoriteFlow = MutableStateFlow<State<Any>>(State.Loading)
    val deleteFavorite: StateFlow<State<Any>> = _deleteFavoriteFlow

    val favorites = mutableListOf<Movie>()

    init {
        viewModelScope.launch {
            favoriteUseCase.getAllFavorite().collect {
                if (it is State.Success) favorites.addAll(it.data)
            }
        }
    }

    fun insertFavorite(movie: Movie) {
        viewModelScope.launch {
            favoriteUseCase.insertFavorite(movie).collect {
                _insertFavoriteFlow.value = it
            }
        }
    }

    fun deleteFavorite(movie: Movie) {
        viewModelScope.launch {
            favoriteUseCase.deleteFavorite(movie).collect {
                _deleteFavoriteFlow.value = it
            }
        }
    }

}
