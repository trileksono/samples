package com.example.miniprojecttest.view.listmovie

import androidx.lifecycle.viewModelScope
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.domain.usecase.MovieUseCase
import com.example.miniprojecttest.view.common.BaseViewModel
import com.example.miniprojecttest.view.listmovie.item.CategoryType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val movieUseCase: MovieUseCase,
) : BaseViewModel() {

    private val _moviePageFlow = MutableStateFlow<State<List<Movie>>>(State.Success(listOf()))
    val moviePageFlow: StateFlow<State<List<Movie>>> = _moviePageFlow

    fun getMoviePage(page: Int, type: CategoryType) {
        viewModelScope.launch {
            movieUseCase.getMoviePage(page, type).collect {
                _moviePageFlow.value = it
            }
        }
    }
}
