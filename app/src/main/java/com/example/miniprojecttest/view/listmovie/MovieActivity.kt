package com.example.miniprojecttest.view.listmovie

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.miniprojecttest.R
import com.example.miniprojecttest.databinding.ActivityMovieBinding
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.view.common.BaseActivity
import com.example.miniprojecttest.view.common.item.LoadingItem
import com.example.miniprojecttest.view.common.item.MovieItem
import com.example.miniprojecttest.view.detail.MovieDetailActivity
import com.example.miniprojecttest.view.favoritemovie.FavoriteActivity
import com.example.miniprojecttest.view.listmovie.bottomsheet.CategoryBottomSheet
import com.example.miniprojecttest.view.listmovie.item.CategoryType
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.activity_movie.*
import kotlinx.android.synthetic.main.content_error_retry.view.*
import kotlinx.coroutines.flow.collect

class MovieActivity : BaseActivity<ActivityMovieBinding, MovieViewModel>() {

    private lateinit var fastAdapter: GenericFastItemAdapter
    private lateinit var footerAdapter: GenericItemAdapter
    private lateinit var endlessScroll: EndlessRecyclerOnScrollListener

    private var categoryContent = arrayListOf(
        Triple("Popular", CategoryType.POPULAR, true),
        Triple("Upcoming", CategoryType.UPCOMING, false),
        Triple("Top Rated", CategoryType.TOP_RATED, false),
        Triple("Now Playing", CategoryType.NOW_PLAYING, false)
    )

    override fun getUiBinding(): ActivityMovieBinding {
        return ActivityMovieBinding.inflate(layoutInflater)
    }

    override fun bindViewModel(): MovieViewModel {
        return ViewModelProvider(this, viewModelFactory).get(MovieViewModel::class.java)
    }

    override fun enableBackButton(): Boolean = false

    override fun bindToolbar(): Toolbar? = viewBinding?.appbars?.toolbars

    override fun onFirstLaunch(savedInstanceState: Bundle?) {
        initRecyclerView()
        viewModel?.getMoviePage(1, CategoryType.POPULAR)
    }

    private fun initRecyclerView() {
        viewBinding?.run {
            this.appbars.toolbars.title = getText(R.string.app_name)
            fastAdapter = FastItemAdapter()
            footerAdapter = ItemAdapter.items()
            fastAdapter.addAdapter(1, footerAdapter)

            endlessScroll =
                object : EndlessRecyclerOnScrollListener(footerAdapter) {
                    override fun onLoadMore(currentPage: Int) {
                        viewModel?.getMoviePage(currentPage + 1,
                            categoryContent.filter { it.third }.map { it.second }.first()
                        )
                    }
                }

            movieRecycler.let {
                it.layoutManager = LinearLayoutManager(this@MovieActivity)
                it.adapter = fastAdapter
                it.addOnScrollListener(endlessScroll)
            }
        }
    }

    override fun initUiListener() {
        fastAdapter.onClickListener = { _, _, item, _ ->
            startActivity(MovieDetailActivity.createIntent(this, (item as MovieItem).movie))
            false
        }

        viewBinding?.run {

            movieContentRetry.contentErrorBtnRetry.setOnClickListener {
                viewBinding?.run {
                    movieContentRetry.root.isVisible = false
                    movieRecycler.isVisible = true
                }
                viewModel?.getMoviePage(
                    1,
                    categoryContent.filter { it.third }.map { it.second }.first()
                )
            }

            appbars.toolbarImgFav.setOnClickListener {
                startActivity(FavoriteActivity.createIntent(this@MovieActivity))
            }

            movieTvCategori.setOnClickListener {
                CategoryBottomSheet.newInstance(categoryContent, { type ->
                    categoryContent = ArrayList(categoryContent.mapIndexed { _, triple ->
                        return@mapIndexed if (triple.second == type) {
                            Triple(triple.first, triple.second, true)
                        } else {
                            Triple(triple.first, triple.second, false)
                        }
                    })
                    fastAdapter.clear()
                    viewModel?.getMoviePage(1, type)
                    endlessScroll.resetPageCount(1)
                }).show(supportFragmentManager, "category")
            }
        }
    }

    override fun observeViewModel(viewModel: MovieViewModel?) {
        lifecycleScope.launchWhenStarted {
            viewModel?.moviePageFlow?.collect {
                when (it) {
                    is State.Success -> {
                        footerAdapter.clear()
                        fastAdapter.add(it.data.map { MovieItem(it) })
                    }
                    is State.Loading -> {
                        footerAdapter.clear()
                        footerAdapter.add(LoadingItem())
                    }
                    is State.Error -> {
                        if (fastAdapter.adapterItemCount == 0) {
                            viewBinding?.run {
                                movieContentRetry.root.isVisible = true
                                movieRecycler.isVisible = false
                            }
                            return@collect
                        }
                        showToast(it.message)
                    }
                }
            }
        }
    }
}
