package com.example.miniprojecttest.view.favoritemovie

import androidx.lifecycle.viewModelScope
import com.example.miniprojecttest.domain.model.Movie
import com.example.miniprojecttest.domain.model.State
import com.example.miniprojecttest.domain.usecase.FavoriteUseCase
import com.example.miniprojecttest.view.common.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoriteViewModel @Inject constructor(
    private val favoriteUseCase: FavoriteUseCase
) : BaseViewModel() {

    private val _favoritesFlow = MutableStateFlow<State<List<Movie>>>(State.Loading)
    val favorites: StateFlow<State<List<Movie>>> = _favoritesFlow

    private val _deleteFavoriteFlow = MutableStateFlow<State<Any>>(State.Loading)
    val deleteFavorite: StateFlow<State<Any>> = _deleteFavoriteFlow

    init {
        viewModelScope.launch {
            favoriteUseCase.getAllFavorite().collect {
                _favoritesFlow.value = it
            }
        }
    }

    fun deleteFavorite(movie: Movie, adapterPosition: Int) {
        viewModelScope.launch {
            favoriteUseCase.deleteFavorite(movie).collect {
                if (it is State.Success) {
                    _deleteFavoriteFlow.value = State.Success(Pair(it.data, adapterPosition))
                } else {
                    _deleteFavoriteFlow.value = it
                }
            }
        }
    }
}
