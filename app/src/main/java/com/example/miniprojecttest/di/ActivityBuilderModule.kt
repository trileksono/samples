package com.example.miniprojecttest.di

import com.example.miniprojecttest.view.detail.MovieDetailActivity
import com.example.miniprojecttest.view.detail.MovieDetailModuleProvider
import com.example.miniprojecttest.view.favoritemovie.FavoriteActivity
import com.example.miniprojecttest.view.favoritemovie.FavoriteModuleProvider
import com.example.miniprojecttest.view.listmovie.MovieActivity
import com.example.miniprojecttest.view.listmovie.MovieModuleProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [MovieModuleProvider::class])
    abstract fun bindMovieActivity(): MovieActivity

    @ContributesAndroidInjector(modules = [MovieDetailModuleProvider::class])
    abstract fun bindMovieDetailActivity(): MovieDetailActivity

    @ContributesAndroidInjector(modules = [FavoriteModuleProvider::class])
    abstract fun bindFavoriteActivity(): FavoriteActivity

}
