package com.example.miniprojecttest.di

import android.content.Context
import com.example.miniprojecttest.Apps
import com.example.miniprojecttest.helper.TagInjection
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Named
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        ActivityBuilderModule::class,
        NetworkModule::class,
        ApiModule::class,
        CacheModule::class
    ]
)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Apps): Context {
        return application
    }

    @Named(TagInjection.IO_Scheduler)
    @Provides
    fun provideIoScheduler(): CoroutineDispatcher = Dispatchers.IO

    @Named(TagInjection.UI_Scheduler)
    @Provides
    fun provideUiScheduler(): CoroutineDispatcher = Dispatchers.Main
}
