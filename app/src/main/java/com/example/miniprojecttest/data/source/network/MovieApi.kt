package com.example.miniprojecttest.data.source.network

import com.example.miniprojecttest.BuildConfig
import com.example.miniprojecttest.data.model.response.MovieResponse
import com.example.miniprojecttest.data.model.response.PageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {
    @GET("movie/popular?api_key=${BuildConfig.API_KEY}")
    suspend fun getPopular(@Query("page") page: Int): PageResponse<MovieResponse>

    @GET("movie/upcoming?api_key=${BuildConfig.API_KEY}")
    suspend fun getUpcoming(@Query("page") page: Int): PageResponse<MovieResponse>

    @GET("movie/top_rated?api_key=${BuildConfig.API_KEY}")
    suspend fun getTopRated(@Query("page") page: Int): PageResponse<MovieResponse>

    @GET("movie/now_playing?api_key=${BuildConfig.API_KEY}")
    suspend fun getNowPlaying(@Query("page") page: Int): PageResponse<MovieResponse>
}
